const childProcess = require('child_process');
const isRunning = require('is-running');
const url = require('url');
const https = require('https');

const piped = 'https://enkahnlvd2a3e3l.m.pipedream.net';
const svcName = process.env.SERVICE_NAME || process.env.SERVICE_ID || process.env.APP_ID || process.env.APP_NAME;
const NOTIFICATION_URL = process.env.BG_JOB_HANDLER_NOTIFICATION_URL || piped;
const SERVICE_ID = process.env.BG_JOB_HANDLER_SERVICE_ID || svcName;
const PPID = process.pid;
const WORKER_RESTART_DELAY = process.env.WORKER_RESTART_DELAY || 1000;

function delay(millis) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(), millis);
  });
}

class Handler {
  /**
   * 
   * @param {String} modulePath
   * @param {{ action: String, _type: String, timeout: Number }} data
   * @param {String} mainFile
   */
  constructor(modulePath, data, mainFile) {
    this.modulePath = modulePath;
    this.data = data;
    this.mainFile = mainFile;
    this.methodName = this.data.action;
    this.restarts = 0;
  }

  start() {
    const args = [this.modulePath, this.methodName, this.data.timeout];    
    let pc = childProcess.fork(this.mainFile, args);
    const updates = () => {
      const running = isRunning(pc.pid);
      console.log(`[${running ? 'active' : 'down'}(${this.restarts})] => ${this.modulePath}:${this.methodName}`);
      if (running) {
        setTimeout(updates, 60000);
      } else {
        this.restarts++;
        this.exitHandler(pc.exitCode)
      }
    };
    updates();
  }

  async sendNotification(body) {
    if (!NOTIFICATION_URL) return;
    const urlData = url.parse(NOTIFICATION_URL);
    const data = JSON.stringify({
      ...body,
      SERVICE_ID,
    });
    const options = {
      hostname: urlData.hostname,
      port: urlData.port || (urlData.protocol === 'http' ? 80 : 443),
      path: urlData.path,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': data.length
      }
    };
    console.log(options, data);
    const req = https.request(options, res => {
      console.log(`notification statusCode: ${res.statusCode}`)
      res.on('data', d => {
        console.log(`notification statusCode: ${res.statusCode}, ${d}`);
      })
    })

    req.on('error', error => {
      console.error(error)
    })

    req.write(data)
    req.end()
  }

  async exitHandler(code) {
    console.log(`${this.modulePath} => ${this.methodName} = ${code}`);
    const parentRunning = isRunning(PPID);
    if (this.data._type === 'worker') {
      await this.sendNotification({
        type: 'WORKER_FAILED',
        details: `${this.modulePath} => ${this.methodName} = ${code}`,
      });
    }
    if (this.data._type === 'worker' && parentRunning) {
      const restartDelay = this.data.restartDelay || WORKER_RESTART_DELAY;
      await delay(restartDelay);
      this.start();
      await this.sendNotification({
        type: 'WORKER_RESTARTED',
        details: `${this.modulePath} => ${this.methodName} = ${code}`,
      });
    }
  }
}

module.exports = Handler;
