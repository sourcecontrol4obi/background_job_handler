const { split, forEach, hasIn, keys, get } = require('lodash');
const schedule = require('node-schedule');

const Handler = require('./handler');
const HeartBeat = require('./heartbeat');

const TASK_LIST = process.env.TASK_LIST;
const WORKER_LIST = process.env.WORKER_LIST;

module.exports = {
  start: (root = __dirname) => {
    taskStart(root);
    workerStart(root);
  }
}

// check if called directly
if (require.main === module) {
  directInvokeTask();
}

async function directInvokeTask() {
  let modulePath = process.argv[2];
  let methodName = process.argv[3];  

  const Module = require(modulePath);

  if (Module) {
    if (Module[methodName]) {
      try {
        const heartbeat = new HeartBeat(`${modulePath}:${methodName}`);
        await Module[methodName](heartbeat);
        process.exit(0);
      } catch (error) {
        console.log(error);
        process.exit(-1);
      }
    } else {
      console.warn(`No such method ${methodName} in ${modulePath}`);
    }
  } else {
    console.warn(`could not find module ${modulePath}`);
  }
}

function executor(modulePath, data) {
  const handler = new Handler(modulePath, data, __filename);
  handler.start();
}

function taskStart(root) {
  let tasks = {};

  try {
    tasks = require(`${root}/tasks`);
  } catch (error) {
    console.log(get(error, 'message', 'tasks module not declared'));
  }

  console.log('Task list =>', TASK_LIST);
  console.log('Available Tasks =>', keys(tasks));
  if (TASK_LIST) {
    let parts = split(TASK_LIST, ',');
    forEach(parts, function (taskName) {
      if (hasIn(tasks, taskName)) {
        forEach(tasks[taskName], function (data) {
          schedule.scheduleJob(
            data.rule,
            executor.bind(null, `${root}/tasks/${taskName}`, { _type: 'task', ...data })
          );
          data.invokeImmediate && executor(`${root}/tasks/${taskName}`, { _type: 'task', ...data });
        });
      } else {
        console.log(`task ${taskName} not defined`);
      }
    });
  }
}

function workerStart(root) {
  let workers = {};

  try {
    workers = require(`${root}/workers`);
  } catch (error) {
    console.log(get(error, 'message', 'workers module not declared'));
  }

  console.log('Worker list =>', WORKER_LIST);
  console.log('Available Workers =>', keys(workers));
  if (WORKER_LIST) {
    let parts = split(WORKER_LIST, ',');
    forEach(parts, (workerName) => {
      if (hasIn(workers, workerName)) {
        forEach(workers[workerName], (data) => {
          try {
            executor(`${root}/workers/${workerName}`, { _type: 'worker', ...data });
          } catch (error) {
            console.log(error);
            process.exit(-1);
          }
        });
      } else {
        console.log(`worker ${workerName} not defined`);
      }
    });
  }
}
