const fs = require('fs');

const delay = (time) => new Promise((resolve) => {
  setTimeout(() => resolve(time), time);
});

module.exports = {
  task: async (heartbeat) => {
    heartbeat.start(10000);
    let count = 0;
    while (count < 5) {
      await delay(5000);
      console.log('writing to file');
      fs.writeFileSync('example.txt', new Date().toISOString());
      count++;
      heartbeat.update();
    }
    // throw new Error('task ended');
    while (true) {
      await delay(5000);
      console.log('doing nothing');
    }
  }
};
