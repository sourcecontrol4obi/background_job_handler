### Description
A package to handling background jobs

### Usage
Create tasks and workers folder in the root directory of your project
```js
let handler = require('background_job_handler');
let rootdir = __dirname; //or root where tasks and workers folders are located
handler.start(rootdir);
```

workers folder should contain
- index.js (holds workers configuration)
- yourWorkerFile1.js
- yourWorkerFile2.js
- ...

tasks folder should contain
- index.js (holds tasks configuration)
- yourTaskFile1.js
- yourTaskFile2.js
- ...

**example worker index.js**
```js
module.exports = {
  yourWorkerFile1: [
    { action: 'task1' },
    { action: 'task2' },
    ...
  ],
  yourWorkerFile2: [
    { action: 'task' }
  ]
}
```

**example yourWorkerFile1.js**
```js
module.exports = {
  task1: async () => {
    // handle task1
  },
  task2: async () => {
    // handle task2
  },
  ...
}
```

**example yourWorkerFile2.js**
```js
module.exports = {
  task: async () => {
    // handle task
  },
  ...
}
```

**example task index.js**
**rule**: this is the cron schedule dependent on [node-schedule](https://www.npmjs.com/package/node-schedule)
**invokeImmediate**: this signifies if to execute immediately on start if true else wait for time specified in rule
```js
module.exports = {
  yourTaskFile1: [
    { rule: '10 * * * * *', action: 'task1', invokeImmediate: true },
    { rule: '20 * * * * *', action: 'task2', invokeImmediate: false },
    ...
  ],
  yourTaskFile2: [
    { rule: '30 * * * * *', action: 'task', invokeImmediate: false },
    ...
  ],
  ...
}
```

**example yourTaskFile1.js**
```js
module.exports = {
  task1: async () => {
    // handle task1
  },
  task2: async () => {
    // handle task2
  },
  ...
}
```

**example yourTaskFile2.js**
```js
module.exports = {
  task: async () => {
    // handle task
  },
  ...
}
```

### Enabling and Disabling 
In your env you can enable or disable a task or worker by adding or removing from the *TASK_LIST* and *WORKER_LIST* variables
example .env
```shell
TASK_LIST="yourTaskFile1,yourTaskFile2"
WORKER_LIST="yourWorkerFile1,yourWorkerFile2"
```
To disable a task or worker, remove it from the corresponding list