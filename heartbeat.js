class Heartbeat {
  constructor(name) {
    this.name = name;
    this.timeout = 0;
    this.lastUpdate = Date.now();
  }

  start(timeout) {
    if (timeout) {
      this.timeout = Number(timeout);
    }
    if (Date.now() - this.timeout > this.lastUpdate) {
      throw new Error(`heart beat lost => ${this.name}`);
    } else {
      setTimeout(this.start.bind(this), this.timeout);
      console.log(`heart beat ok => ${this.name}`);
    }
  }

  update() {
    this.lastUpdate = Date.now();
  }
}

module.exports = Heartbeat;
