require('dotenv').config();

const bg = require('.');

bg.start('.');

setInterval(function() {
  console.log("timer that keeps nodejs processing running");
}, 1000 * 60 * 60);